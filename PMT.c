#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <dirent.h>
#include <string.h>


#define MAX_FILES_DIR_AMOUNT 20 //
#define MAX_CHAR_LIMIT 20
#define _DIR 1                  //this is using a macro definition to provide a constant to let us say whether we want to list directories or files (1 for directories - an arbitrary choice)
#define _FILE 2                 //this is using a macro definition to provide a constant to let us say whether we want to list directories or files (2 for files - an arbitrary choice)
#define MAX_NUM 100             //avoiding using dynamically allocated memory, and thus pointers, by using fixed length arrays - why might this not be the best option?
#define NAME_LENGTH 100

int ValidCheck(char STRINGINPUT[MAX_CHAR_LIMIT]);
int Testing();
int InputtingToTree();
int Running();
void tree(char *basePath, const int root);
int Welcome();
int OutputOptions();
int CreateFile();
int CreateFolder();
int ReturnCommand(int CommandNumber);


int main(void)
{
    Running();
    return 0;
}

int OutputOptions()
{
    printf("PM: ==Displaying Options== \n");
    printf("PM: Enter (1) for Create Project \n");
    printf("PM: Enter (2) for Create Folder or Directory \n");
    printf("PM: Enter (3) for Create File \n");
    printf("PM: Enter (4) for Create Feature \n");
    printf("PM: Enter (5) for Add Tag \n");
    printf("PM: Enter (6) for Find Tag \n");
    printf("PM: Enter (7) for Move Tag \n");
    printf("PM: Enter (8) for Output File Structure \n");
    printf("PM: Enter (9) Quit Program \n");
    return 0;
}






int CheckForGaps()
{
    char STRINGINPUT[MAX_CHAR_LIMIT];
    printf("Inputting Character: ");
    fgets(STRINGINPUT, MAX_CHAR_LIMIT, stdin);
    printf("%s", STRINGINPUT);
    for(int i =0; i<MAX_CHAR_LIMIT;i++){
        if(STRINGINPUT[i]==' ')
            printf("SPACE");
    }
}
int ValidCheck(char STRINGINPUT[MAX_CHAR_LIMIT])
{
    printf("Checking: ");
    printf("%s", STRINGINPUT);
    for(int i =0; i<MAX_CHAR_LIMIT;i++){
        if(STRINGINPUT[i]==' ')
            return -1;
    }
    return 0;
}
int InputtingToTree(){
    // Directory path to list files
    char path[100];

    // Input path from user
    printf("Enter path to list files: ");
    scanf("%s", path);

    tree(path, 0);

    return 0;
}
int Running()
{
    //mkdir("Test2/Test1");
    int InputNumber;
    InputNumber = 1;
    do
    {
        OutputOptions();
        printf("PM: Enter an Integer that Corresponds to the command: ");
        scanf("%d",&InputNumber);
        char path[100];
        printf("PM: Confirming Input: %d",InputNumber);
        switch(InputNumber)
        {
            case 1:
                printf("Enter Project Name: \n");
                scanf("%s", path);
                CreateFolder(path);
                break;
            case 2:
                printf("Enter Folder: \n");
                scanf("%s", path);
                CreateFolder(path);
                break;
                printf("Enter File: \n");
                scanf("%s", path);
                CreateFile(path);
            case 3:
                break;
            case 4:
                break;
            case 5:
                break;
            case 6:
                break;
            case 7:
                break;
            case 8:
            printf("PM: Table");
                char * Test = ".";
                for(int i; i < sizeof(Test);i++){
                    path[i]=Test[i];
                }
                printf("%s\n",path);
                printf("%s\n",Test);
                tree(path, 0);
                tree(path, 0);
                break;
            case 9:
                break;
            default:
                printf("PM: Error Invalid Input \n");
                break;
        }  
    }
    while(InputNumber<9);
    return 0;
}
int Testing(){

}
int DoesFileExist(char *basePath, const int root)
{   
    
    int i;
    char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(".");

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            strcpy(path, basePath);
            strcat(path, "/");
            strcat(path, dp->d_name);
            printf("%s\n",dp->d_name);
            tree(path, root + 2);

        }

    }

    closedir(dir);
}
void tree(char *basePath, const int root)
{
    int i;
    char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(basePath);

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            for (i=0; i<root; i++) 
            {
                if (i%2 == 0 || i == 0)
                    printf("%c", 179);
                else
                    printf(" ");

            }

            printf("%c%c%s\n", 195, 196, dp->d_name);

            strcpy(path, basePath);
            strcat(path, "/");
            strcat(path, dp->d_name);
            tree(path, root + 2);
        }
    }

    closedir(dir);
}
int Welcome(){
    printf("Welcome to Henry's Project Management Tool");
    return 0;
}

int CreateFile(char *FileName)
{
    FILE * fpointer = fopen(FileName,"w");

    return 0;
}
int CreateFolder(char *DirName){
    printf("Creating Folder");
    mkdir(DirName);
    return 0;
}

/*    fprintf(fpointer, "Jim, Sales\nPam, Receptionist\nOscar, Accounting");
    // r is read the file
    // w is write onto the file
    // a is append onto the file
    fclose(fpointer);

    char Line[255];
    FILE * fpointer2 = fopen("Employees.txt","r");
    fgets(Line,255,fpointer2);
    printf("%s",Line);
    fclose(fpointer2);
        int Num = 1;
    char Word[MAX_CHAR_LIMIT];
    char TestWord[]= "Hello";
    for(int i; i < sizeof(Word);i++){
        Word[i]=TestWord[i];
    }
    Num = ValidCheck(Word);
    printf("%d",Num);
    printf("T Word %s",TestWord);
    printf("Word %s",Word);
    return 0;
    InputtingToTree();
    return 0;
    Welcome();
    Running();
    printf("Last Command");
    return 0;
*/
//Reference List
/* https://codeforwin.org/2018/03/c-program-to-list-all-files-in-a-directory-recursively.html
* https://overiq.com/c-programming-101/array-of-strings-in-c/
*
*
*/